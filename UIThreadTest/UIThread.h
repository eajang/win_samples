#pragma once
#include "EventDlg.h"


// CUIThread
#define WM_UPDATE_UI WM_USER+20
class CUIThread : public CWinThread
{
	DECLARE_DYNCREATE(CUIThread)

public:
	CUIThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CUIThread();

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

protected:
	DECLARE_MESSAGE_MAP()

public:
	void RegistEventUICallback(void* handler, void(*pFunc)(void *pObj, int picID));

	int m_eventID;
	virtual int Run();

	void* m_hDlg;
	void(*m_pCbFunc)(void *pObj, int picID);
};
