#pragma once
#include "afxwin.h"


// EventDlg 대화 상자입니다.

class EventDlg : public CDialogEx
{
	DECLARE_DYNAMIC(EventDlg)

public:
	EventDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~EventDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_EVENT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT OnUpdateGestureEvent(WPARAM wParam, LPARAM lParam);

public:
	CStatic m_staticEventUI;
	virtual BOOL OnInitDialog();
};
