
// UIThreadTestDlg.h : header file
//
#include "EventDlg.h"
#include "UIThread.h"
#pragma once


// CUIThreadTestDlg dialog
class CUIThreadTestDlg : public CDialogEx
{
// Construction
public:
	CUIThreadTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_UITHREADTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//EventDlg* eventDlg;
	CUIThread* m_pUIThread;

public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButton2();
	static void UpdateEventUICallback(void *pObj, int eventID);
};
