
// UIThreadTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UIThreadTest.h"
#include "UIThreadTestDlg.h"
#include "UIThread.h"
#include "afxdialogex.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
//	virtual BOOL DestroyWindow();
//	virtual BOOL OnInitDialog();
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CUIThreadTestDlg dialog



CUIThreadTestDlg::CUIThreadTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CUIThreadTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUIThreadTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CUIThreadTestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CUIThreadTestDlg::OnBnClickedButton1)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON2, &CUIThreadTestDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// CUIThreadTestDlg message handlers

BOOL CUIThreadTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	//eventDlg = new EventDlg();
	//eventDlg->Create(IDD_EVENT_DIALOG);
	//eventDlg->ShowWindow(SW_SHOW);
	m_pUIThread = (CUIThread*)AfxBeginThread(RUNTIME_CLASS(CUIThread));
	m_pUIThread->RegistEventUICallback(this, CUIThreadTestDlg::UpdateEventUICallback);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CUIThreadTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CUIThreadTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CUIThreadTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

#define WM_UPDATE_GESTURE_EVENT WM_USER+1
#define WM_UPDATE_UI WM_USER+20
void CUIThreadTestDlg::OnBnClickedButton1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	
	//HWND hwnd = eventDlg->GetSafeHwnd();
	//::SendMessage(hwnd, WM_UPDATE_GESTURE_EVENT, 0, 0);
	m_pUIThread->PostThreadMessageW(WM_UPDATE_UI, 1, 0);

}


void CUIThreadTestDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
	//if (eventDlg != NULL)
	//{
	//	eventDlg->DestroyWindow();
	//	delete eventDlg;
	//	eventDlg = NULL;
	//}
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	if (WaitForSingleObject(m_pUIThread->m_hThread, 2000) == WAIT_TIMEOUT)
	{
		m_pUIThread->SuspendThread();
		m_pUIThread->ExitInstance();
		delete m_pUIThread;
		m_pUIThread = NULL;
	}
}



void CUIThreadTestDlg::OnBnClickedButton2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_pUIThread->PostThreadMessageW(WM_UPDATE_UI, 2, 0);
}

void CUIThreadTestDlg::UpdateEventUICallback(void *pObj, int eventID)
{
	CUIThreadTestDlg* pDlg = (CUIThreadTestDlg*)pObj;
	switch (eventID)
	{
	case 1:
		OutputDebugString(_T("1111\n"));
		Sleep(3000);
		OutputDebugString(_T("111---1\n"));
		break;
	case 2:
		OutputDebugString(_T("2222\n"));
		break;
	}
	// update...
}