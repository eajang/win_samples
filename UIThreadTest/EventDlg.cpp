// EventDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UIThreadTest.h"
#include "EventDlg.h"
#include "afxdialogex.h"

#define WM_UPDATE_GESTURE_EVENT WM_USER+1
// EventDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(EventDlg, CDialogEx)

EventDlg::EventDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(EventDlg::IDD, pParent)
{
	Create(IDD_EVENT_DIALOG);
}

EventDlg::~EventDlg()
{
}

void EventDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_EVENT_PIC, m_staticEventUI);
}


BEGIN_MESSAGE_MAP(EventDlg, CDialogEx)
	ON_MESSAGE(WM_UPDATE_GESTURE_EVENT, &EventDlg::OnUpdateGestureEvent)
//	ON_WM_CLOSE()
END_MESSAGE_MAP()


// EventDlg 메시지 처리기입니다.


afx_msg LRESULT EventDlg::OnUpdateGestureEvent(WPARAM wParam, LPARAM lParam)
{
	CBitmap image, image2;
	image.LoadBitmapW(IDB_IMG_HAND_DETECT);
	m_staticEventUI.SetBitmap(image);
	SetLayeredWindowAttributes(RGB(0, 0, 0), 255, LWA_COLORKEY | LWA_ALPHA);
	Sleep(2000);
	OutputDebugString(_T("test"));
	return true;
}


//BOOL EventDlg::OnInitDialog()
//{
//	CDialogEx::OnInitDialog();
//
//	// TODO:  여기에 추가 초기화 작업을 추가합니다.
//
//	return TRUE;  // return TRUE unless you set the focus to a control
//	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
//}


BOOL EventDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	SetWindowLong(this->GetSafeHwnd()
		, GWL_EXSTYLE
		, GetWindowLong(this->GetSafeHwnd(), GWL_EXSTYLE) | WS_EX_LAYERED);
	this->SetBackgroundColor(RGB(0, 0, 0));
	SetLayeredWindowAttributes(RGB(0, 0, 0), 255, LWA_COLORKEY | LWA_ALPHA);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
