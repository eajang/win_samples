// UIThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UIThreadTest.h"
#include "UIThread.h"


// CUIThread

IMPLEMENT_DYNCREATE(CUIThread, CWinThread)

CUIThread::CUIThread()
{
}

CUIThread::~CUIThread()
{
}

BOOL CUIThread::InitInstance()
{
	// TODO:  여기에서 각 스레드에 대한 초기화를 수행합니다.
	m_bAutoDelete = TRUE;
	m_eventID = 0;	// NO_DETECTION
	return TRUE;
}

int CUIThread::ExitInstance()
{
	// TODO:  여기에서 각 스레드에 대한 정리를 수행합니다.

	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CUIThread, CWinThread)
END_MESSAGE_MAP()


void CUIThread::RegistEventUICallback(void* handler, void(*pFunc)(void *pObj, int picID))
{
	m_hDlg = handler;
	m_pCbFunc = pFunc;
}

int CUIThread::Run()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	MSG msg;
	while (true)
	{
			if (::GetMessage(&msg, NULL, 0, 0) != NULL)
			{
				if (msg.message == WM_UPDATE_UI)
				{
					m_pCbFunc(m_hDlg, msg.wParam);
				}
			}
		Sleep(1000);	
	}
	
	return CWinThread::Run();
}
